package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"gitlab.com/mvenezia/knative-helloworld-go/pkg/version"
)

func main() {
	flag.Parse()
	log.Print("Application started...")

	http.HandleFunc("/", homepageHandler)
	http.HandleFunc("/version", versionHandler)
	http.ListenAndServe(":8080", nil)
}

func homepageHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("Homepage Requested")
	fmt.Fprintf(w, "Hello World!\n")
	log.Print("Homepage Served")
}

func versionHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("Version Requested")
	info := version.Get()
	fmt.Fprintf(w,"Version Information:\n")
	fmt.Fprintf(w,"\tGit Data:\n")
	fmt.Fprintf(w,"\t\tTagged Version:\t%s\n", info.GitVersion)
	fmt.Fprintf(w,"\t\tHash:\t\t%s\n", info.GitCommit)
	fmt.Fprintf(w,"\t\tTree State:\t%s\n", info.GitTreeState)
	fmt.Fprintf(w,"\tBuild Data:\n")
	fmt.Fprintf(w,"\t\tBuild Date:\t%s\n", info.BuildDate)
	fmt.Fprintf(w,"\t\tGo Version:\t%s\n", info.GoVersion)
	fmt.Fprintf(w,"\t\tCompiler:\t%s\n", info.Compiler)
	fmt.Fprintf(w,"\t\tPlatform:\t%s\n\n", info.Platform)
	log.Print("Version Served")
}

